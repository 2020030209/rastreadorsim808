
function initMap() {
  var mapOptions = {
    center: { lat: 0, lng: 0 }, // Centrar el mapa en coordenadas iniciales
    zoom: 15 // Nivel de zoom del mapa
  };
  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

  // Recibir los datos del servidor (reemplaza "TU_SERVIDOR" por la dirección de tu servidor)
  fetch('http://127.0.0.1:3000/map')
    .then(response => response.json())
    .then(data => {
      // Obtener las coordenadas de latitud y longitud del archivo JSON recibido
      var lat = data.lat;
      var lon = data.lon;

      // Crear un marcador en el mapa con las coordenadas recibidas
      var marker = new google.maps.Marker({
        position: { lat: parseFloat(lat), lng: parseFloat(lon) },
        map: map
      });

      // Centrar el mapa en las coordenadas recibidas
      map.setCenter({ lat: parseFloat(lat), lng: parseFloat(lon) });
    })
    .catch(error => console.error('Error al recibir los datos:', error));
}
