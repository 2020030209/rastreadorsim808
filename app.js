const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2');


const app = express();
const PORT = 2002;

// Middleware para analizar el cuerpo de las solicitudes en formato JSON
app.use(bodyParser.json());

// Configuración de la conexión a la base de datos MySQL
const db = mysql.createConnection({
  host: '54.144.47.225',     // Cambia esto por la dirección de tu base de datos
  user: 'root',
  password: 'Admin123',
  database: 'PetFindDB'
});


// Crear una tabla si no existe
db.query(`CREATE TABLE IF NOT EXISTS ubicacion (
  id INT AUTO_INCREMENT PRIMARY KEY,
  IdMascota INT NOT NULL,
  Lati FLOAT NOT NULL,
  Longi FLOAT NOT NULL,
  timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)`, (error, results) => {
  if (error) {
    console.error('Error al crear la tabla:', error);
  } else {
    console.log('Tabla creada exitosamente o ya existente');
  }
});

// Ruta para recibir solicitudes POST en /map y insertar los datos en la base de datos
app.post('/map', (req, res) => {
  const receivedData = req.body;
  console.log('Datos recibidos:', receivedData);

  // Insertar las ubicacion en la base de datos
  const { latitude, longitude } = receivedData;
  db.query('INSERT INTO ubicacion (IdMascota, Lati, Longi) VALUES (1, ?, ?)', [latitude, longitude], (error, results) => {
    if (error) {
      console.error('Error al insertar las ubicacion:', error);
      res.status(500).json({ error: 'Error al insertar las ubicacion' });
    } else {
      res.json({ status: 'success', message: 'Datos insertados correctamente' });
    }
  });
});

// Servir el archivo HTML estático
app.use(express.static('public'));

// Iniciar el servidor en el puerto especificado
app.listen(PORT, () => {
  console.log(`Servidor escuchando en el puerto ${PORT}`);
});